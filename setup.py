""" gym-schemaverse packaging instructions """

from setuptools import setup, find_packages

setup(
    name='gym_schemaverse',
    packages=['gym-schemaverse'],
    version='0.1',
    description='OpenAI environment for Schemaverse,'
    author='FrozenFOXX',
    author_email='frozenfoxx@churchoffoxx.net',
    url='https://github.com/frozenfoxx/gym-schemaverse',
    download_url='https://github.com/frozenfoxx/gym-schemaverse/archive/0.1.tar.gz',
    keywords=['schemaverse', 'postgresql', 'machinelearning'],
    classifiers=[],
    install_requires=[
        'gym'
    ],
)
