import configparser
import docker
import gym
from gym import error, spaces, utils
from gym.utils import seeding
from .armada_controller import ArmadaController
from .game_controller import GameController
from .player_controller import PlayerController
import inspect
import itertools
import numpy as np
import os
import sys
import yaml
import logging

# Set up logger
logger = logging.getLogger(__name__)

# Load default config
config = yaml.safe_load(open(os.path.join(os.path.dirname(inspect.stack()[0][1]), "schemaverse_docker_config.yml")))

class SchemaverseDockerEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self):

        # Set up the value ranges from config
        scoreboard_min = [ config['PLAYER_ID_MIN'], config['DAMAGE_TAKEN_MIN'], config['DAMAGE_DONE_MIN'], config['PLANETS_CONQUERED_MIN'], config['PLANETS_LOST_MIN'], config['SHIPS_BUILT_MIN'], config['SHIPS_LOST_MIN'], config['SHIP_UPGRADES_MIN'], config['DISTANCE_TRAVELLED_MIN'] ]
        scoreboard_max = [ config['PLAYER_ID_MAX'], config['DAMAGE_TAKEN_MAX'], config['DAMAGE_DONE_MAX'], config['PLANETS_CONQUERED_MAX'], config['PLANETS_LOST_MAX'], config['SHIPS_BUILT_MAX'], config['SHIPS_LOST_MAX'], config['SHIP_UPGRADES_MAX'], config['DISTANCE_TRAVELLED_MAX'] ]
        shipboard_min = [ config['SHIP_ID_MIN'], config['PLAYER_ID_MIN'], config['LAST_ACTION_TIC_MIN'], config['LAST_MOVE_TIC_MIN'], config['LAST_LIVING_TIC_MIN'], config['CURRENT_HEALTH_MIN'], config['MAX_HEALTH_MIN'], config['CURRENT_FUEL_MIN'], config['MAX_FUEL_MIN'], config['MAX_SPEED_MIN'], config['RANGE_MIN'], config['ATTACK_MIN'], config['DEFENSE_MIN'], config['ENGINEERING_MIN'], config['PROSPECTING_MIN'], config['LOCATION_X_MIN'], config['LOCATION_Y_MIN'], config['DIRECTION_MIN'], config['SPEED_MIN'], config['DESTINATION_X_MIN'], config['DESTINATION_Y_MIN'], config['REPAIR_PRIORITY_MIN'], config['ACTION_MIN'], config['ACTION_TARGET_MIN'], config['TARGET_SPEED_MIN'] ]
        shipboard_max = [ config['SHIP_ID_MAX'], config['PLAYER_ID_MAX'], config['LAST_ACTION_TIC_MAX'], config['LAST_MOVE_TIC_MAX'], config['LAST_LIVING_TIC_MAX'], config['CURRENT_HEALTH_MAX'], config['MAX_HEALTH_MAX'], config['CURRENT_FUEL_MAX'], config['MAX_FUEL_MAX'], config['MAX_SPEED_MAX'], config['RANGE_MAX'], config['ATTACK_MAX'], config['DEFENSE_MAX'], config['ENGINEERING_MAX'], config['PROSPECTING_MAX'], config['LOCATION_X_MAX'], config['LOCATION_Y_MAX'], config['DIRECTION_MAX'], config['SPEED_MAX'], config['DESTINATION_X_MAX'], config['DESTINATION_Y_MAX'], config['REPAIR_PRIORITY_MAX'], config['ACTION_MAX'], config['ACTION_TARGET_MAX'], config['TARGET_SPEED_MAX'] ]

        self.action_space = spaces.Tuple((
                                    spaces.Box(0.0,5.0, 1), # action (attack, create, mine, move, repair, upgrade)
                                    spaces.Box(config['LOCATION_X_MIN'],config['LOCATION_X_MAX'], 1), # target x (all)
                                    spaces.Box(config['LOCATION_Y_MIN'],config['LOCATION_Y_MAX'], 1), # target y (all)
                                    ))
        self.observation_space = spaces.Tuple((
                                    spaces.Box(np.array(scoreboard_min),np.array(scoreboard_max), 1), # scoreboard
                                    spaces.Box(np.array(shipboard_min),np.array(shipboard_max), 1), # shipboard
                                    spaces.Box(np.array(config['TIC_MIN']), np.array(config['TIC_MAX']), 1), # tic
                                    spaces.Box(-5.0, 0.0, 1),  # learning rate
                                    spaces.Box(-7.0, -2.0, 1),  # decay
                                    spaces.Box(2, 8, 1),  # batch size
                                    spaces.Box(-6.0, 1.0, 1),  # l1 reg
                                    spaces.Box(-6.0, 1.0, 1),  # l2 reg
                                    spaces.Box(0.0, 1.0, (5, 2)),  # convolutional layer parameters
                                    spaces.Box(0.0, 1.0, (2, 2)),  # fully connected layer parameters
                                    ))

        self.docker_client = docker.from_env()
        self.game_server = ''
        self.game_port = config['PORT_NUMBER']
        self.players = {}
        self.playersconf_location = ''
        self.port = ''
        self._configure_environment()

        # Set up the Postgres connection string
        conn_string = ''

        # Build the game environment controllers from the DB connection
        self.armada_controller = ArmadaController(conn_string)
        self.game_controller = GameController(conn_string)
        self.player_controller = PlayerController(conn_string)
        self.tic = 0

        # Gym variables
        self.status = ''
        self.reset_count = 0
        self.step_count = 0
        self.running = True
        self.episode_over = False

    @property
    def action_space(self):
        """ Refactor the available action space to ships and planets """

        # Build a list of ships from the observation

        # Loop through each ship, adding to the list of ships

        # Set an action space per ship
        ship_action_space = spaces.Tuple((
                                spaces.Box(0.0,5.0, 1), # action (attack, create, mine, move, repair, upgrade)
                                spaces.Box(config['LOCATION_X_MIN'],config['LOCATION_X_MAX'], 1), # target x (all)
                                spaces.Box(config['LOCATION_Y_MIN'],config['LOCATION_Y_MAX'], 1), # target y (all)
                                ))

        # Retrieve a list of owned planets

        # Add each planet to the list but with only one action to choose
        planet_action_space = spaces.Tuple((
                                spaces.Box(1.0,1.0, 1), # action (create)
                                spaces.Box(config['LOCATION_X_MIN'],config['LOCATION_X_MAX'], 1), # planet x (all)
                                spaces.Box(config['LOCATION_Y_MIN'],config['LOCATION_Y_MAX'], 1), # planet y (all)
                                ))


        return action_space_list

    def __del__(self):
        """ Deletes the environment """

        self._unload_container()

    def _configure_environment(self):
        """ Loader for subclasses """

        self._load_container()
        self._load_players()

    def _load_container(self):
        """ Loads the container """

        self.game_server = self.docker_client.containers.run('frozenfoxx/schemaverse:latest',
                                                             detach=True,
                                                             auto_remove=True,
                                                             publish_all_ports=True
                                                            )

        # Retrieve exposed port
        self.port = self.docker_client.api.port(self.game_server.id, self.game_port)[0]['HostPort']

        # Check until DB is ready
        ready = False
        while not ready:
            check_output = str(self.game_server.exec_run('/usr/bin/pg_isready'))
            ready = 'accepting connections' in check_output

    def _load_players(self):
        """ Loads the players into the game server """

        playersconf = configparser.ConfigParser()

        try:
            playersconf.read(self.playersconf_location)
        except Exception as e:
            sys.exit("Unable to read playersconf file, does it exist?")

        for k in playersconf['DEFAULT']:
            self.players[k] = playersconf['DEFAULT'][k]

        # Iterate over players to add to game server
        for i in self.players:
            player_add = "/src/schemaverse/scripts/add_player.sh " \
                + str(i) \
                + " " \
                + str(self.players[i])

            self.game_server.exec_run(player_add)

    def _status(self):
        """ Reports the status of the container """

        return self.game_server.status

    def _unload_container(self):
        """ Kills the container """

        self.game_server.stop()

    def close(self):
        self.__del__

    def _step(self, action):
        """ Perform actions in the environment """

        self._act(action)
        obs = self._observe()
        self.episode_over = self._evaluate_end_state()
        reward = self._get_reward()

        self.step_count += 1
        return obs, reward, self.episode_over, {}

    def _act(self, action, count=1):
        for _ in itertools.repeat(None, count):
            self.player_controller.send_command(action)

    def _observe(self):
        """ Observe the environment """

        # Update variables

        self.game_controller.update_variables()

        # Update tic, if it hasn't advanced just return the existing space
        if self.game_controller.update_tic() <= self.tic:
            return self.observation_space

        # Update players
        self.game_controller.update_players()

        # Update planets
        # TODO: this is going to be very slow, don't update every time
        self.game_controller.update_planets()

        # Update list of ships
        self.armada_controller.update()

        # Update scoreboard
        self.game_controller.update_scoreboard()

        # Calculate and set new observation space


    
    def _evaluate_end_state(self):
        """ Check to see if game is over """

        return False

    def _reset(self):
        """ Resets the environment """

        self.reset_count += 1
        self.step_count = 0
        self._unload_container()
        self._configure_environment()

        return self._observe()

    def _render(self, mode='human', close=False):
        print("Render here")
