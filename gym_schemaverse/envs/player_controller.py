""" Controller for a player """

import sys
import psycopg2
from psycopg2.extras import DictCursor
from .armada_controller import ArmadaController
from .game_controller import GameController
from .player import MyPlayer

class PlayerController(object):
    """ Handle all player inputs """

    def __init__(self, conn_string):

        # Build database connection
        self.conn = psycopg2.connect(conn_string)
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor) #create database cursor. parameter grabs result as dict (which is not default)
        self.conn.autocommit = True #disable transactions (transactions left uncommitted will hang the game)

        self.myplayer = MyPlayer(self.cur)
        self.game_controller = GameController(self.cur)
        self.armada = ArmadaController(self.cur)

    def disconnect(self, args):
        """ Disconnect from the server """

        # Close down database connection
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def mystatus(self, args):
        """ Status of the player """
        """   Returns a List with the username, balnace, and fuel_reserve """

        self.myplayer.update()

        return [self.myplayer.username, self.myplayer.balance, self.myplayer.fuel_reserve]

    def planets(self, args):
        """ Retrieve the list of planets """

        # Check if table scan is necessary
        if self.game_controller.count_planets() != len(self.game_controller.planets):
            self.game_controller.update_planets()

        # Return the list
        #   Each item: id, name, mine_limit, location_x,
        #     location_y, conqueror
        return self.game_controller.planets

    def planets_in_range(self, args):
        """ Dump table of planets in range """

        # Return the list
        #   Each item: ship id, planets
        return self.armada.planets_in_range()

    def planets_update(self, args):
        """ For an update for the planets """

        self.game_controller.update_planets()

    def players(self, args):
        """ Show all players """

        self.game_controller.update_players()

        # Return the list
        #  Each item: id, username, dmg taken, dmg done, planets conq,
        #    planets lost, ships built, ships lost, ship upgrades,
        #    distance traveled, fuel mined
        return self.game_controller.players

    def send_command(self, args):
        """ Receive a command for execution """

        # Determine which command from the lookup table

        # Determine arguments for command

        # Execute

    def ship_attack(self, args):
        """ Attack a target ship with your own ships
            ship_attack [ship id] [target id] """

        # Check argument list
        if len(args) == 0:
            print("[-] Error: argument list cannot be empty")
        elif len(args.split()) != 2:
            print("[-] Error: argument list incorrect")
        else:
            arglist = args.split()
            self.armada.attack(arglist[0], arglist[1])

    def ship_create(self, args):
        """ Create a ship
            ship_create [name]
            ship_create [name] [attack] [defense] [engineering] [prospecting] [location] """

        if len(args) == 0:
            print("[-] Error: argument list cannot be empty")
        elif len(args) > 6:
            print("[-] Error: argument list too long")

        else:
            # Set ship name
            name = args.split(' ')[0]

            # Set ship attrs and location if offered
            stats = []
            if len(args.split(' ')) > 1:
                stats.append(args.split(' ')[1])
                stats.append(args.split(' ')[2])
                stats.append(args.split(' ')[3])
                stats.append(args.split(' ')[4])
                location = args.split(' ')[5]
                self.armada.create(name, stats, location)
            else:
                stats.append(5)
                stats.append(5)
                stats.append(5)
                stats.append(5)
                location = ""
                self.armada.create(name, stats, location)

    def ship_mine(self, args):
        """ Order a ship in range of a planet to mine
            ship_mine [ship id] [planet id] """

        # Check argument list
        if len(args) == 0:
            print("[-] Error: argument list cannot be empty")
        elif len(args.split()) != 2:
            print("[-] Error: argument list incorrect")
        else:
            arglist = args.split()
            self.armada.mine(arglist[0], arglist[1])

    def ship_move(self, args):
        """ Move a ship at a speed towards a point
            ship_move [ship id] [max|half] [location_x] [location_y] """

        # Check argument list
        if len(args) == 0:
            print("[-] Error: argument list cannot be empty")
        elif len(args.split()) != 4:
            print("[-] Error: argument list incorrect")
        else:
            arglist = args.split()
            self.armada.move(arglist[0], arglist[1], arglist[2], arglist[3])

    def ship_repair(self, args):
        """ Order a ship in range of a target ship to repair
            ship_repair [source ship id] [target ship id] """

        # Check argument list
        if len(args) == 0:
            print("[-] Error: argument list cannot be empty")
        elif len(args.split()) != 2:
            print("[-] Error: argument list incorrect")
        else:
            arglist = args.split()
            self.armada.repair(arglist[0], arglist[1])

    def ship_upgrade(self, args):
        """ Upgrade an attribute of a ship
            ship_upgrade [id] [attribute] [quantity]
            Attributes:
            MAX_HEALTH  MAX_FUEL  MAX_SPEED  RANGE
            ATTACK  DEFENSE  ENGINEERING PROSPECTING """

        # Check argument list
        if len(args) == 0:
            print("[-] Error: argument list cannot be empty")
        elif len(args.split()) != 3:
            print("[-] Error: argument list incorrect")
        else:
            arglist = args.split()
            for ship in self.armada.ships:
                if ship.ship_id == int(arglist[0]):
                    ship.upgrade(arglist[1], arglist[2])

    def ships(self, args):
        """ Show ships """

        self.armada.update()

        # Return the list
        #  Each item (Ship): id, fleet_id, player_id, name, last_action_tic,
        #    last_move_tic, last_living_tic, current_health, max_health,
        #    current_fuel, max_fuel, max_speed, range, attack, defense,
        #    engineering, prospecting, location_x, location_y,
        #    direction, speed, destination_x, destination_y,
        #    repair_priority, action, action_target_id, location,
        #    destination, target_speed, target_direction
        return self.armada.ships

    def ships_in_range(self, args):
        """ Dump table of ships in range """

        # Return the dictionary
        #   Each item: ship id, target ship id(s)
        return self.armada.ships_in_range()
