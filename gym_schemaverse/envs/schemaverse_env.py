import gym
from gym import error, spaces, utils
from gym.utils import seeding
import inspect
import numpy as np
import os
import sys
import yaml
import logging

# Set up logger
logger = logging.getLogger(__name__)

# Load default config
config = yaml.safe_load(open(os.path.join(os.path.dirname(inspect.stack()[0][1]), "schemaverse_config.yml")))

class SchemaverseEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self, obs_min=0, obs_max=1e9):
        scoreboard_min = [ obs_min, obs_min, obs_min, obs_min, obs_min, obs_min, obs_min, obs_min, obs_min, obs_min, obs_min ]
        scoreboard_max = [ obs_max, obs_max, obs_max, obs_max, obs_max, obs_max, obs_max, obs_max, obs_max, obs_min, obs_min ]

        self.action_space = spaces.Tuple()
        self.game_server = ''
        self.observation_space = spaces.Box(np.array(scoreboard_min), np.array(scoreboard_max))
        self.user = config['USER']
        self.password = config['PASSWORD']
        self.host = config['HOST']
        self.port = config['PORT']
        self.status = ''
        self._configure_environment()

    def __del__(self):
        """ Deletes the environment """

    def _configure_environment(self):
        """ Loader for subclasses """

    def _status(self):
        """ Reports the status of the connection """

    def close(self):
        self.__del__

    def step(self, action):
        print("Step here")

    def reset(self):
        """ Resets the environment """

    def render(self, mode='human', close=False):
        print("Render here")
