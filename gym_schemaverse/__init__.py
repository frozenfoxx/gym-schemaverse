from gym.envs.registration import register

register(
    id='schemaverse-v0',
    entry_point='gym_schemaverse.envs:SchemaverseEnv'
)

register(
    id='schemaverse-docker-v0',
    entry_point='gym_schemaverse.envs:SchemaverseDockerEnv'
)