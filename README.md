# gym-schemaverse
Schemaverse environment for OpenAI Gym.

# Requirements
* Docker
* Python 3+

# Installation

```
cd gym-schemaverse
pip install -e .
```
